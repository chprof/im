'use strict';
var gulp = require('gulp'),
	postcss = require('gulp-postcss'),
	prefixer = require('autoprefixer'),
	sass = require('gulp-sass'),
	maps = require('gulp-sourcemaps'),
	concat = require('gulp-concat'),
	mqpacker = require('css-mqpacker'),
	watch = require('gulp-watch'),
	rigger = require('gulp-rigger'),
	uglify = require('gulp-uglify'),
	cssnano = require('cssnano'),
	main_files = require('main-bower-files'),
	// imgmin = require('gulp-imagemin'),
	// jpgmin = require('imagemin-jpegtran'),
	// pngmin = require('imagemin-pngquant'),
	browserSync = require('browser-sync'),
	reload = browserSync.reload,
	mediaSort = require('sort-css-media-queries');
gulp.task('browserSync', function() {
	browserSync({
		server: {
		baseDir: "./dist"
	},
	port: 3000,
	open: true,
	notify: false,
	tunnel: true
	});
});
gulp.task('style', function() {
	var processors = [
		prefixer({
			browsers: ['last 10 version'],
			cascade: false
		}),
		mqpacker({
			sort: mediaSort
		}),
		cssnano({
            preset: 'default',
        }),
	];
	return gulp.src('app/scss/**/main.scss')
		.pipe(maps.init())
		.pipe(sass({
			outputStyle: 'expanded',
			errLogToConsole: true
		}).on('error', sass.logError))
		.pipe(postcss(processors))
		.pipe(maps.write('maps/'))
		.pipe(gulp.dest('dist/css'))
		.pipe(reload({stream:true}));
});
gulp.task('js', function() {
	return gulp.src('app/js/**/*.js')
		.pipe(maps.init())
		.pipe(uglify())
		.pipe(maps.write('maps/'))
		.pipe(gulp.dest('dist/js'))
		.pipe(reload({stream:true}));
});
gulp.task('html', function(){
  return gulp.src(['app/**/*.html', '!app/templates', '!app/templates/**'])
  .pipe(rigger())
  .pipe(gulp.dest('dist/'))
  .pipe(reload({stream:true}));
});
gulp.task('images', function() {
	// var img_set = [
	//  jpgmin({
	//      progressive: true
	//     }),
	//     pngmin({
	//      quality: '70-90',
	//      speed: 1,
	//      floyd: 1
	//     })
	// ];
	return gulp.src('app/images/**/*')
		// .pipe(imgmin(img_set))
		.pipe(gulp.dest('dist/images/'))
		.pipe(reload({stream:true}));
})
// gulp.task('awesome', function() {
// 	return gulp.src('bower/components-font-awesome/fonts/fontawesome-webfont.*')
// 		.pipe(gulp.dest('app/fonts/'));
// });
gulp.task('fonts', function() {
	return gulp.src('app/fonts/**/*')
		.pipe(gulp.dest('dist/fonts/'))
		.pipe(reload({stream:true}));
});
gulp.task('build', [
	'style',
	'js',
	'html',
	'images',
	'fonts'
]);

gulp.task('main-style-libs', function() {
	return gulp.src(main_files('**/*.css'))
		.pipe(gulp.dest('app/scss/libs/'))
});
gulp.task('main-js-libs', function() {
	return gulp.src(main_files('**/*.js'))
		.pipe(gulp.dest('app/js/libs/'))
});
gulp.task('concat-style-libs', function() {
	return gulp.src('app/scss/libs/*')
		.pipe(concat('vendor.css'))
		.pipe(postcss([
			cssnano({
	            preset: 'default',
	        })
		]))
		.pipe(gulp.dest('dist/css/'))
});
gulp.task('bower', [
	'main-style-libs',
	'main-js-libs'
]);
gulp.task('concatcss', ['concat-style-libs']);

gulp.task('watcher', function(){
	watch('app/scss/**/*.scss', function(event, cb) {
		gulp.start('style');
	});
	watch('app/scss/libs/*', function(event, cb) {
		gulp.start('concatcss');
	}); 
	// watch('app/css/**/*', function(event, cb) {
	// 	gulp.start('css-replace');
	// }); 
	watch('app/js/**/*.js', function(event, cb) {
		gulp.start('js');
	}); 
	watch('app/**/*.html', function(event, cb) {
		gulp.start('html');
	}); 
	watch('app/images/**/*', function(event, cb) {
		gulp.start('images');
	}); 
	watch('app/fonts/**/*', function(event, cb) {
		gulp.start('fonts');
	}); 
});
gulp.task('default', ['watcher', 'build', 'browserSync']);
